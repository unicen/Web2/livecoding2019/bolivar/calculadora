<?php
function getHome() {
    $html = '<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="css/style.css">
        <title>Calculadora</title>
        <script src="https://code.jquery.com/jquery-git.min.js"></script>
    </head>
    <body>
    
        <nav id="main-nav">
            <ul>
                <li><a href="home">Calculadora</a></li>
                <li><a href="pi">El número Pi</a></li>
                <li><a href="about">About</a></li>
            </ul>
        </nav>
        
        <h1>Calculadora</h1>
    
        <form id="form-calc">
            <input type="number" name="operando1">
            <input type="number" name="operando2">
            <select name="operacion">
                <option value="suma">Sumar</option>
                <option value="resta">Restar</option>
                <option value="multiplicacion">Multiplicar</option>
                <option value="division">División</option>
            </select>
            <input type="submit" value="CALCULAR">
        </form>
        <p id="resultado"></p>
    
        <script src="js/main.js"></script>
    </body>
    </html>';

    return $html;
}
?>