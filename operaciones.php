<?php

function sumar($a, $b) {
    $resultado = $a + $b;
    return $resultado;
}

function restar($a, $b) {
    $resultado = $a - $b;
    return $resultado;
}

function multiplicar($a, $b) {
    $resultado = $a * $b;
    return $resultado;
}

function dividir($a, $b) {
    $resultado = $a / $b;
    return $resultado;
}