<?php

function showPiNumber() {
    echo "<h1>El número Pi</h1>";
    echo "<p> π (pi) es la relación entre la longitud de una circunferencia y su diámetro en geometría euclidiana.​ Es un número irracional y una de las constantes matemáticas más importantes. Se emplea frecuentemente en matemáticas, física e ingeniería.</p>";
    echo "<p>El valor es: <strong>" . pi() . "<strong></p>";
}
?>