<?php
function showAbout($developer) {
$html = '<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <base href="http://'.$_SERVER['SERVER_NAME'] . dirname($_SERVER['PHP_SELF']).'/'.'" target="_blank">
    <link rel="stylesheet" href="css/style.css">
    <title>Calculadora | About</title>
</head>
<body>

    <nav id="main-nav">
        <ul>
            <li><a href="index.html">Calculadora</a></li>
            <li><a href="pi">El número Pi</a></li>
            <li><a href="about">About</a></li>
        </ul>
    </nav>

    <h1>Acerca de nosotros...</h1>

    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ex voluptatem, saepe accusantium eum iusto distinctio ea accusamus, unde totam blanditiis eveniet temporibus quos debitis ullam non vel dolor, dolores tenetur!</p>
    
    <ul>
        <li><a href="about/emanuel">Emanuel Volpe</a></li>
        <li><a href="about/pedro">Pedro Lopez</a></li>
        <li><a href="about/paula">Paula Casado</a></li>
    </ul>';

    if (isset($developer)) { 
        if ($developer == "emanuel") {
            $html .= "<h2> Emanuel Volpe </h2><p>Lorem... </p><img src='img/img_avatar_h.png'/>";
        } elseif ($developer == "paula") {
            $html .= "<h2> Paula Casado </h2><p>Lorem... </p><img src='img/img_avatar_m.png'/>";
        } elseif (($developer == "pedro")) {
            $html .= "<h2> Pedro Lopez </h2><p>Lorem... </p><img src='img/img_avatar_h.png'/>";
        }
    }
    $html .= '</body>
    </html>';

    return $html;
}

?>