"use strict"

document.querySelector("#form-calc").addEventListener("submit", event => {
    event.preventDefault();
    let operacion = document.querySelector("select[name=operacion]").value;
    let operando1 = document.querySelector("input[name=operando1]").value;
    let operando2 = document.querySelector("input[name=operando2]").value;
    
    fetch(operacion + "/" + operando1 +"/" + operando2)
    .then(response => response.text())
    .then(html => {
        document.getElementById("resultado").innerHTML = "El resultado es: " + html;
    })
    .catch(error => console.log(error));
});
