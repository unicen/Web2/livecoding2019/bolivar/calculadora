<?php
    require_once('pi.php');
    require_once('about.php');
    require_once('operaciones.php');
    require_once('home.php');

    if ($_GET['action'] == '')
        $_GET['action'] = 'home';
    
    $partesURL = explode('/', $_GET['action']);
    //$partesURL[0] -> accion (pi, about, suma, division, resta, multiplicacion)
    //$partesURL[1...N] -> parametros.

    switch ($partesURL[0]) {
        case 'about':
            $developer = null;
            if (isset($partesURL[1]))
                $developer = $partesURL[1];
            echo showAbout($developer);
            break;
        case 'pi' :
            showPiNumber();
            break;
        case 'suma' :
            echo sumar($partesURL[1], $partesURL[2]);
            break;
        case 'resta' :
            echo restar($partesURL[1], $partesURL[2]);
            break;
        case 'division':
            echo dividir($partesURL[1], $partesURL[2]);
            break;
        case 'multiplicacion':
            echo multiplicar($partesURL[1], $partesURL[2]);
            break;
        default:
            echo getHome();
            break;
    }
    
?>